#!/usr/bin/python

# Copyright (c) 2009 Christoph Berg <myon@debian.org>
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The name of the author may not be used to endorse or promote products
#    derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
# THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import cgi, psycopg2

form = """Content-Type: text/html; charset=utf-8

<html>
<head>
<title>dcontrol</title>
</head>
<body>
<h1>dcontrol</h1>
<p>
Query package and source control files for all Debian distributions.
(Aka "remote apt-cache")
</p>

<p>
<form>
<table>
<tr>
 <td>archive</td>
 <td>suite</td>
 <td>component</td>
 <td>architecture</td>
 <td>package</td>
 <td>version</td>
 <td colspan="2">show suite</td>
</tr>
<tr>
 <td><select name="archive">%(archive)s</select></td>
 <td><select name="suite">%(suite)s</select></td>
 <td><select name="component">%(component)s</select></td>
 <td><select name="architecture">%(architecture)s</select></td>
 <td><input name="package"></td>
 <td><input name="version"></td>
 <td><input type="checkbox" name="annotate"></td>
 <td><input type="submit" value="Get"></td>
</tr>
</table>
</form>
</p>

<p>
Most recent packages/sources files used: <br />
%(last_update)s
</p>

<p>
<em>Notes:</em>
The only mandatory field is <em>package</em>. Leaving other fields blank will
select all matching distributions. A blank <em>architecture</em> field selects
both binary and source packages. The 'binary' architecture excludes source
packages. Selecting a specific architecture will also include <em>Architecture:
all</em> packages in that distribution. Checking the <em>show suites</em> box
adds headers showing which distribution the control file is from. In the
output, source packages have a <em>Binary:</em> field that binary packages do
not have.
</p>

<p>
<em>Made by Christoph Berg &lt;myon@debian.org&gt;</em>
</p>

</body>
</html>
"""

def dropdown(kind, table):
    cur.execute("""SELECT DISTINCT %(kind)s FROM %(table)s ORDER BY %(kind)s""" % \
            dict(kind=kind, table=table))
    things = [['']] + cur.fetchall()
    list = [ "<option>%s</option>" % thing[0] for thing in things ]
    return "".join(list)

def lookup(package, conditions):
    if conditions.has_key('annotate') and conditions['annotate'][0]:
        fields = "control, archive, suite, component"
    else:
        fields = "control"
    sql = """SELECT DISTINCT %s
             FROM package_control
             JOIN package USING (package_id)
             JOIN packagelist USING (package_id)
             JOIN suite USING (suite_id)
             WHERE package = %%s""" % fields
    args = [package]
    if conditions.has_key('archive'):
        sql += " AND archive = %s"
        args.append(conditions['archive'][0])
    if conditions.has_key('suite'):
        sql += " AND suite = %s"
        args.append(conditions['suite'][0])
    if conditions.has_key('component'):
        sql += " AND component = %s"
        args.append(conditions['component'][0])
    if conditions.has_key('architecture'):
        if conditions['architecture'][0] == 'binary':
            sql += " AND architecture <> 'source'"
        else:
            sql += " AND (architecture = %s OR pkg_architecture = %s)" # 'all' hack
            args.append(conditions['architecture'][0])
            args.append(conditions['architecture'][0])
    if conditions.has_key('version'):
        sql += " AND version = %s"
        args.append(conditions['version'][0])

    cur.execute(sql, args)

    newline = 0
    for item in cur.fetchall():
        if newline:
            print
        if conditions.has_key('annotate') and conditions['annotate'][0]:
            print "Archive:", item[1]
            print "Suite:", item[2]
            print "Component:", item[3]
        print item[0]
        newline = 1

pg = psycopg2.connect('service=qa user=guest')
cur = pg.cursor()
cur.execute("SET search_path TO apt")
cur.execute("SET timezone TO 'UTC'")

request = cgi.parse()

if request.has_key('package'):
    print """Content-Type: text/plain; charset=utf-8\n"""
    lookup(request['package'][0], request)

else:
    cur.execute("""SELECT archive, MAX (last_update)
                   FROM suite GROUP BY archive ORDER BY archive""")
    last_update = [ "%s: %s UTC <br />" % suite for suite in cur.fetchall() ]
    dict = {
        'archive': dropdown('archive', 'suite'),
        'suite': dropdown('suite', 'suite'),
        'component': dropdown('component', 'suite'),
        'architecture': dropdown('architecture', 'architecture'),
        'last_update': "\n".join(last_update)
    }
    print form % dict
