BEGIN;

SET search_path TO apt;

-- distribution table

DELETE FROM apt.distribution;

COPY apt.distribution (archive, suite, distribution) FROM STDIN WITH DELIMITER ' ';
debian buster oldoldstable
debian buster-proposed-updates oldoldstable-proposed-updates
debian-security buster oldoldstable-security
debian buster-updates oldoldstable-updates
debian-backports buster-backports oldoldstable-backports
debian-backports buster-backports-sloppy oldoldstable-backports-sloppy
debian bullseye oldstable
debian bullseye-proposed-updates oldstable-proposed-updates
debian-security bullseye oldstable-security
debian bullseye-updates oldstable-updates
debian bullseye-backports oldstable-backports
debian bookworm stable
debian bookworm-proposed-updates stable-proposed-updates
debian-security bookworm stable-security
debian bookworm-updates stable-updates
debian trixie testing
debian trixie-proposed-updates testing-proposed-updates
debian-security trixie testing-security
debian trixie-updates testing-updates
debian sid unstable
debian experimental experimental
\.

COMMIT;
