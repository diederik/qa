#!/usr/bin/perl

####################
#    Copyright (C) 2008 by Raphael Geissert <atomo64@gmail.com>
#
#
#    This file is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This file is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this file.  If not, see <https://www.gnu.org/licenses/>.
####################

use strict;
use warnings;
use lib '/usr/share/devscripts';
use Devscripts::Debbugs;

my ($bugs);
my @output;
my (@itp, @rfp, @o, @ita, @rfa, @rfh, %wnpp);
my @wnpp_status = qw(itp rfp o ita rfa rfh);
%wnpp = (itp=>\@itp, rfp=>\@rfp, o=>\@o, ita=>\@ita, rfa=>\@rfa, rfh=>\@rfh);

for (@wnpp_status) {
    $wnpp{$_} = Devscripts::Debbugs::select('package:wnpp', 'status:open',
		'user:wnpp@packages.debian.org', 'usertag:' . $_);
}

sub devscripts_debbugs_status # TODO revert this once a newer devscripts does this
{
	my $bugs = shift;
	my %status;

	while( my @part = splice( @$bugs, 0, 1000 ) )
	{
		my $partstatus = Devscripts::Debbugs::status (\@part);
		%status = ( %status, %$partstatus );
	}

	return \%status;
}

#$bugs = Devscripts::Debbugs::status(
$bugs = devscripts_debbugs_status(
	    Devscripts::Debbugs::select('package:wnpp', 'status:open'));


for (keys %{$bugs}) {
    my $bug = $bugs->{$_};

    if ($bug->{'subject'} =~ m/^(ITP|RFP|O|ITA|RFA|RFH):\s+([\w.+-]+)\s+\-\-\s+(.+)$/) {
	my ($status, $pkg, $desc) = ($1, $2, $3);
	$status = lc($status);
	my @tags2remove;
	for (@wnpp_status) {
	    next if ($_ eq $status);
	    push @tags2remove, $_
		if (grep $_ eq $bug->{'id'}, @{$wnpp{$_}});
	}
	push @output, "usertags $bug->{'id'} - @tags2remove"
	    if (@tags2remove);
	push @output, "usertags $bug->{'id'} + $status"
	    unless (grep $_ eq $bug->{'id'}, @{$wnpp{$status}});
    } else {
	my @tags2remove;
	for (@wnpp_status) {
	    push @tags2remove, $_
		if (grep $_ eq $bug->{'id'}, @{$wnpp{$_}});
	}
	push @output, "usertags $bug->{'id'} - @tags2remove"
	    if (@tags2remove);
    }
}

print "package wnpp\n";
if (@output) {
    local $" = "\n";
    print "user wnpp\@packages.debian.org\n";
    print "@output\n";
}
print "thanks\n";
